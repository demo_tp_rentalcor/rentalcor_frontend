import App from './App.vue'
import Vue from 'vue'
import router from './router'
import store from './store'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
require('./assets/css/main.css')
import axios from 'axios'
import VueAxios from 'vue-axios'
import AutocompleteVue from 'autocomplete-vue';
import vuetify from './plugins/vuetify';



Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.component('autocomplete-vue', AutocompleteVue);

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
