import Vue from 'vue'
import Router from 'vue-router'

import homePage from './components/Search.vue'
import quoteDetails from './components/QuoteDetails.vue'
import myQuotes from './components/MyQuotes.vue'

const myRutes = [
    {
        path: '*',
        component: homePage
    },
    {
        path: '/',
        component: homePage
    },
    {
        path: '/quote',
        component: quoteDetails
    },
    {
        path: '/myquotes',
        component: myQuotes
    }
]

Vue.use(Router)

const router = new Router({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes: myRutes
  })
  
  
  export default router;