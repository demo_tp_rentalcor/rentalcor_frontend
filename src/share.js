import axios from 'axios';

export const swal = require('sweetalert2');

export const httpBackend = axios.create({
    baseURL: '/rentalcorbackend/'
});