import Vue from 'vue';
import Vuex from 'vuex';
import { httpBackend } from "../share";

Vue.use(Vuex);

const state = {
    quoteData: {},
    myQuotes: [],
};

export default new Vuex.Store({
    state: state,
    mutations: {
        setQuoteData(state, value) {
            state.quoteData = value;
        },
        setMyQuotes(state, value) {
            state.myQuotes = value;
        }
    },
    actions: {
        getMyQuotes({ commit }) {
            httpBackend.get('/quotes')
                .then(res => {
                    commit('setMyQuotes', res.data.quotes)
                })
                .catch(error => { console.log('error: ', error) })
                .finally(() => { })
        }
    },
    getters: {
        hasUserYuli: state => {
            //Cuando alguien esta en pending, debe retornar false, porque es como si volviera a empezar
            return (state.searchResult.users.length > 0 && (state.searchResult.users[0].status && (state.searchResult.users[0].status != 'P' && state.searchResult.users[0].status != 'PE' && state.searchResult.users[0].status != 'PD'))) ? true : false;
        },

    },
    modules: {

    }
})