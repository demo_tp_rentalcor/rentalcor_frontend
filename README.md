# Trabajo Práctico IAEW

## Technologies:
- Node
- VueJS
- Docker
- Docker Compose
  
## How to run:
- `npm install`
- `npm run serve`

## Local port:

- 8080

## Docker port:

- 80