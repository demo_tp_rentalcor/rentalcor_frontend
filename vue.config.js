module.exports = {
  devServer: {
    proxy: {
      '/rentalcorbackend': {
        target: 'http://localhost:3000/api',
        secure: false,
        pathRewrite: { '^/rentalcorbackend' : '' }
      }
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}